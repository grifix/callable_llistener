<?php
declare(strict_types=1);

namespace Grifix\CallableListener\Tests\Stub;

final class Listener
{

    public function __construct(public ?string $value = null)
    {
    }

    public function notHandlingMethod($value):void{

    }

    public function onEvent(Event $event): void
    {
        $this->value = $event->value;
    }


}
