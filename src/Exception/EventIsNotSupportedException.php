<?php
declare(strict_types=1);

namespace Grifix\CallableListener\Exception;

use Exception;

final class EventIsNotSupportedException extends Exception
{
    public function __construct(string $eventClass, string $listenerClass)
    {
        parent::__construct(sprintf('Event [%s] is not supported by listener [%s]!', $eventClass, $listenerClass));
    }
}
