<?php
declare(strict_types=1);

namespace Grifix\CallableListener;

use Grifix\CallableListener\Exception\EventIsNotSupportedException;

final class CallableListener
{
    private array $handlingMethods = [];

    private \ReflectionClass $reflectionClass;

    public function __construct(private readonly object $listener)
    {
        $this->reflectionClass = new \ReflectionClass($listener);
    }

    public function __invoke(object $event): void
    {
        $method = $this->detectHandlingMethod($event::class);
        $this->listener->$method($event);
    }

    private function detectHandlingMethod(string $eventClass): string
    {
        if (isset($this->handlingMethods[$eventClass])) {
            return $this->handlingMethods[$eventClass];
        }
        foreach ($this->reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {
            if ($eventClass === $this->getEventClass($method)) {
                $this->handlingMethods[$eventClass] = $method->getName();
                return $this->handlingMethods[$eventClass];
            }
        }
        throw new EventIsNotSupportedException($eventClass, $this->listener::class);
    }

    public function isEventSupported($eventClass): bool
    {
        try {
            $this->detectHandlingMethod($eventClass);
        } catch (EventIsNotSupportedException) {
            return false;
        }
        return true;
    }

    private function getEventClass(\ReflectionMethod $method): ?string
    {
        $type = $method->getParameters()[0]->getType();
        if (null !== $type) {
            return $type->getName();
        }
        return null;
    }
}
